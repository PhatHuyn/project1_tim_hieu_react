import React from "react";
import HeadComponent from "./HeadComponent";
import BodyComponent from "./BodyComponent";
import FooterComponent from "./FooterComponent";

export default function DemoComponent() {
  return (
    <div className="text-center">
      {/* <HeadComponent /> */}
      <BodyComponent />
      <FooterComponent />
    </div>
  );
}
