import React, { Component } from "react";

export default class BodyComponent extends Component {
  handleClick = () => {
    alert("Hello Phát!!!");
  };

  render() {
    return (
      <div>
        <p>BodyComponent</p>
        <button onClick={this.handleClick}>Click me</button>
      </div>
    );
  }
}
