import React from "react";

export default function FooterComponent() {
  const handleClick = (content) => {
    alert(content);
  };
  return (
    <div className="mt-10 bg-dark text-white">
      <p>FooterComponent</p>
      <button
        onClick={() => {
          handleClick("Hello Phat");
        }}
      >
        Click me
      </button>
    </div>
  );
}
