import React from "react";

export default function HeadComponent({ Component }) {
  return (
    <div>
      <div className="mt-10 bg-warning">
        <p>HeadComponent</p>
      </div>
      <Component />
    </div>
  );
}
