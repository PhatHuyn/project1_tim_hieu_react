import React, { useEffect, useState } from "react";

export default function DemoState() {
  const [value, setValue] = useState(1);
  useEffect(() => {
    console.log("Hàm được thực thi sau mỗi lần render");
    console.log(
      "Cách viết này tương ứng với cả 2 lifeCycle didMount và didUpdate"
    );
  });
  useEffect(() => {
    console.log("Hàm được sau lần render đầu tiên");
    return () => {
      console.log(
        "Hàm định nghĩa bên trong đây sẽ được gọi cuối cùng thay WillUnmount"
      );
    };
  }, []);
  useEffect(() => {
    console.log(
      "Hàm gọi mỗi khi value (state) thay đổi sau khi render ! thay didUpdate"
    );
  }, [value]);

  let handleGiam = () => {
    setValue(value - 1);
  };
  return (
    <div>
      {console.log("render")}
      <button onClick={handleGiam}>Giam Value</button>
      <p>{value}</p>
      <button onClick={() => setValue(value + 1)}>Tang Value</button>
    </div>
  );
}
