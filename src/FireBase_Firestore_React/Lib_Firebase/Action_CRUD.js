import { addDoc, deleteDoc, doc, getDocs, updateDoc } from "firebase/firestore";
import { movieCollectionRef } from "./firestore.collection";
import { getListMovieAction } from "../../DemoRedux/action/getListMovieAction";
import { db } from "./init_firebase";

//Lấy danh sách fim từ firestore về
export const getMovies = (dispatch) => {
  getDocs(movieCollectionRef)
    .then((res) => {
      const movs = res.docs.map((doc) => ({ data: doc.data(), id: doc.id }));
      dispatch(getListMovieAction(movs));
    })
    .catch((err) => {
      console.log(err.message);
    });
};

// Thêm movie vào database
export const addMovie = (name, dispatch) => {
  addDoc(movieCollectionRef, { name })
    .then((res) => {
      console.log(res.id);
      getMovies(dispatch);
    })
    .catch((err) => {
      console.log(err.message);
    });
  alert(name);
};

// Chỉnh sữa movie
export const editMovie = (id, name, dispatch) => {
  //Đọc dữ liệu của id truyền vào
  const docRef = doc(db, "movies", id);
  //Cập nhật
  updateDoc(docRef, { name })
    .then((res) => {
      console.log(res);
      alert("Update thành công");
      getMovies(dispatch);
    })
    .catch((err) => {
      console.log(err.massage);
    });
};

//Xoá dữ liệu
export const deleteMovie = (id, dispatch) => {
  const docRef = doc(db, "movies", id);
  deleteDoc(docRef)
    .then((res) => {
      alert("Delete thành công");
      getMovies(dispatch);
    })
    .catch((err) => {
      console.log(err.message);
    });
};
