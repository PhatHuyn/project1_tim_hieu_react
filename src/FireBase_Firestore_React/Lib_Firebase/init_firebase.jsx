// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCyR9yefDmLNbpRcoUvrhC1GQEHZVNtPCc",
  authDomain: "fir-firebase-react-35d2d.firebaseapp.com",
  projectId: "fir-firebase-react-35d2d",
  storageBucket: "fir-firebase-react-35d2d.appspot.com",
  messagingSenderId: "908401854771",
  appId: "1:908401854771:web:3966bb8fff6cac00675646",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize firestore (lấy database từ firestore về)
export const db = getFirestore(app);
