import { collection } from "firebase/firestore";
import { db } from "./init_firebase";

export const movieCollectionRef = collection(db, "movies");
