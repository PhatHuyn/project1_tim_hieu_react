import React, { useEffect } from "react";
import ListMovies from "./components/ListMovies";
import AddMovies from "./components/AddMovies";
import EditMovies from "./components/EditMovies";
import { useDispatch } from "react-redux";
import { getMovies } from "./Lib_Firebase/Action_CRUD";

export default function React_Firebase() {
  const dispatch = useDispatch();
  useEffect(() => {
    getMovies(dispatch);
  }, []);

  return (
    <div>
      <h1 className="text-center bg-warning h-100">React_Firebase</h1>
      <ListMovies />
      <AddMovies />
      <EditMovies />
    </div>
  );
}
