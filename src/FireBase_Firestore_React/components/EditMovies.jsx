import React from "react";
import { Button, Form, Input } from "antd";
import { editMovie } from "../Lib_Firebase/Action_CRUD";
import { useDispatch } from "react-redux";

export default function EditMovies() {
  const dispatch = useDispatch();
  const onFinish = (values) => {
    console.log(values);
    const name = values.name;
    const id = values.id;
    if (name === "" || id === "") {
      return;
    } else {
      //Cập nhật dữ liệu lên database
      editMovie(id, name, dispatch);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <h4>EditMovies</h4>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 400,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="ID Movies"
          name="id"
          id="id"
          rules={[
            {
              required: true,
              message: "Please input!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Name Movies"
          name="name"
          id="name"
          rules={[
            {
              required: true,
              message: "Please input!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
