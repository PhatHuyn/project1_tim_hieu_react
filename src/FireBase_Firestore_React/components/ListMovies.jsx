import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteMovie } from "../Lib_Firebase/Action_CRUD";

export default function ListMovies() {
  const dispatch = useDispatch();
  const movies = useSelector((state) => {
    return state.movieReducer.movies;
  });

  //   useEffect(() => {
  //     const unSub = getMovies2();
  //     return () => {
  //       unSub();
  //     };
  //   }, []);

  //Lấy danh sách fim từ firestore về (RealTime)
  //   function getMovies2() {
  //     onSnapshot(movieCollectionRef, (snapshot) => {
  //       setMovies(snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() })));
  //     });
  //   }

  return (
    <div>
      <h4>ListMovies</h4>
      <ul className="mt-2">
        {movies.map((movie) => (
          <li className="mt-2" key={movie.id}>
            <button
              className="mr-2 btn btn-danger"
              onClick={() => {
                deleteMovie(movie.id, dispatch);
              }}
            >
              Xoá
            </button>
            id: {movie.id} - name: {movie.data.name}
          </li>
        ))}
      </ul>
    </div>
  );
}
