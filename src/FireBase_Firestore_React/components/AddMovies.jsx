import React from "react";
import { Button, Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { addMovie } from "../Lib_Firebase/Action_CRUD";

export default function AddMovies() {
  const dispatch = useDispatch();
  const onFinish = (values) => {
    const name = values.name;
    if (name === " ") {
      return;
    } else {
      // Thêm movies vào database
      addMovie(name, dispatch);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <h4>AddMovies</h4>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 400,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Name Movies"
          name="name"
          id="name"
          rules={[
            {
              required: true,
              message: "Please input!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
