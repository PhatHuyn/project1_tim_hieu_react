import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { giamSoLuongAction, tangSoLuongAction } from "./action/numberAction";

export default function DemoFunctionRedux() {
  const dispatch = useDispatch();

  const Statenumber = useSelector((state) => {
    return state.numberReducer.soLuong;
  });

  useEffect(() => {
    console.log(Statenumber);
  }, []);

  return (
    <div className="text-center py-5">
      <button
        onClick={() => {
          dispatch(giamSoLuongAction(10));
        }}
        className="btn btn-danger"
      >
        -
      </button>

      <span className="display-4 text-secondary mx-3">{Statenumber}</span>

      <button
        onClick={() => {
          dispatch(tangSoLuongAction());
        }}
        className="btn btn-warning"
      >
        +
      </button>
    </div>
  );
}
