import { TANG_SO_LUONG } from "../constants/numberConstants";

export let tangSoLuongAction = () => {
  return {
    type: TANG_SO_LUONG,
  };
};

export let giamSoLuongAction = (value) => {
  return {
    type: "GiamSoLuong",
    payload: value,
  };
};
