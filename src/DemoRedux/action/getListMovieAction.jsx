import { GET_LIST_MOVIE } from "../constants/movieConstants";

export let getListMovieAction = (value) => {
  return {
    type: GET_LIST_MOVIE,
    payload: value,
  };
};
