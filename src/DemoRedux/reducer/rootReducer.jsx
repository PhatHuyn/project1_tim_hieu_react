import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";
import { movieReducer } from "./movieReducer";

export let rootReducer_DemoRedux = combineReducers({
  numberReducer,
  movieReducer,
});
