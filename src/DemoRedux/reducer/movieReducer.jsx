import { GET_LIST_MOVIE } from "../constants/movieConstants";

let initialState = {
  movies: [],
};

export let movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_MOVIE: {
      state.movies = action.payload;
      return { ...state };
    }

    default: {
      return state;
    }
  }
};
