import "./App.css";
import DemoComponent from "./DemoComponent/DemoComponent";
import DemoProps from "./DemoProps/DemoProps";
import DemoFunctionRedux from "./DemoRedux/DemoFunctionRedux";
import DemoRedux from "./DemoRedux/DemoRedux";
import DemoState from "./DemoState/DemoState";
import HeadComponent from "./DemoComponent/HeadComponent";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "antd/dist/reset.css";
import DemoRouter from "./DemoRouter/DemoRouter";
import BodyComponent from "./DemoComponent/BodyComponent";
import React_Firebase from "./FireBase_Firestore_React/React_Firebase";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        {/* <DemoComponent />;
        <DemoProps />;
        <DemoRedux />
        <DemoFunctionRedux /> */}
        {/* <DemoState />; */}
        <Routes>
          <Route path="/" element={<React_Firebase />} />
          <Route path="/demoComponent" element={<DemoComponent />} />
          <Route
            path="/demo"
            element={<HeadComponent Component={DemoRouter} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
