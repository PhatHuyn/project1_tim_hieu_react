import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class DemoRouter extends Component {
  render() {
    return (
      <div>
        <NavLink to={"/demoComponent"}>
          <button>Chuyển sang DemoComponet</button>
        </NavLink>
      </div>
    );
  }
}
