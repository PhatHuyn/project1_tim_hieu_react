import React from "react";

export default function FunctionPropsChild(props) {
  return (
    <div>
      <p>FunctionPropsChild</p>
      <h2>Username : {props.userName}</h2>

      <h3>Age : {props.userAge}</h3>

      <button
        onClick={() => {
          props.handleChangeName("Phat");
        }}
        className="btn btn-danger"
      >
        Change name
      </button>
    </div>
  );
}
