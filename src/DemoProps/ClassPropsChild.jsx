import React, { Component } from "react";

export default class ClassPropsChild extends Component {
  render() {
    return (
      <div>
        <p>ClassPropsChild</p>
        <h2>Username : {this.props.userName}</h2>

        <h3>Age : {this.props.userAge}</h3>

        <button
          onClick={() => {
            this.props.handleChangeName("Phat");
          }}
          // anfn
          className="btn btn-danger"
        >
          Change name
        </button>
      </div>
    );
  }
}
