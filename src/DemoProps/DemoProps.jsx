import React, { Component } from "react";
import ClassPropsChild from "./ClassPropsChild";
import FunctionPropsChild from "./FunctionPropsChild";

export default class DemoProps extends Component {
  state = {
    username: "Alice",
    age: 2,
  };

  handleChangeName = (name) => {
    this.setState({ username: name });
  };

  render() {
    return (
      <div>
        <p>DemoProps</p>
        <FunctionPropsChild
          userAge={this.state.age}
          userName={this.state.username}
          handleChangeName={this.handleChangeName}
        />
      </div>
    );
  }
}
