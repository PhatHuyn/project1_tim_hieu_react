import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
// Set up Redux
import { Provider } from "react-redux";
import { createStore } from "redux";

// Root reducer là file mình tạo ra nó là store tổng của toàn bộ ứng dụng (nó chứa có reducer con)
import { rootReducer_DemoRedux } from "./DemoRedux/reducer/rootReducer";

let store = createStore(
  rootReducer_DemoRedux,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

reportWebVitals();
